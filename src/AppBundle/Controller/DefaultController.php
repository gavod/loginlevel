<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class DefaultController extends Controller
{

    public function loginAction(Request $request)
    {
         $form = $this->createFormBuilder()
             ->setAction($this->generateUrl('login'))
             ->setMethod('POST')
             ->add('Username')
             ->add('Password')
             ->add('Login',SubmitType::class)
             ->getForm();

         $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

           $details = $form->getData();

           $em = $this->getDoctrine()->getManager();
           
           $userCheck = $em->getRepository('AppBundle:User')->findOneBy(array('username' => $details['Username'],'password' => $details['Password']));

            if(empty($userCheck)) {
                return $this->render('default/LoginForm.html.twig',array('form' => $form->createView(),'message' => 'Either Email or Password is not correct'));
            } else {
                #TODO create session for user level
                // get route from db so can use 1 page, 1 function, and add different user levels
                $userLevel = $userCheck->getLevelId()->getDestinationPage();
                return $this->redirect("/$userLevel");

            }

        }

         return $this->render('default/LoginForm.html.twig',array('form' => $form->createView(),'message' => ''));
    }

    public function userAreaAction($userLevel) {
        #TODO check user session
        return $this->render('default/AreaForm.html.twig',array('level' => $userLevel));

    }
}
