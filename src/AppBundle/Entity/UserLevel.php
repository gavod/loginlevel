<?php

namespace AppBundle\Entity;
use AppBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
/**
 * UserLevel
 */
class UserLevel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $level;

    /**
     * @var string
     */
    private $destinationPage;


    /**
     * Get id
     *
     * @return int
     */

    public function getId()
    {
        return $this->id;
    }

    /**
     * Set level
     *
     * @param string $level
     *
     * @return UserLevel
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return string
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set destinationPage
     *
     * @param string $destinationPage
     *
     * @return UserLevel
     */
    public function setDestinationPage($destinationPage)
    {
        $this->destinationPage = $destinationPage;

        return $this;
    }

    /**
     * Get destinationPage
     *
     * @return string
     */
    public function getDestinationPage()
    {
        return $this->destinationPage;
    }
}

